const colorList = ["pallet", "viridian", "pewter", "cerulean", "vermillion", "lavender","celadon","saffron","fuschia","cinnabar"];
var stock=colorList[0];
// tạo html show button
let createColorButton = () => {
    let buttonTag0=`<button id="${colorList[0]}" class="color-button ${colorList[0]} active" onclick="changeColor('${colorList[0]}')"></button>`;
    let buttonList=buttonTag0;
    for (let i=1; i<colorList.length; i++){
        let buttonTag=`<button id="${colorList[i]}" class="color-button ${colorList[i]}" onclick="changeColor('${colorList[i]}')"></button>`;
        buttonList=buttonList+buttonTag;
    };
    document.getElementById('colorContainer').innerHTML=buttonList;
};
// chạy lần đầu khi load trang
createColorButton();
// đổi màu nhà + class active cho tagname
let changeColor = (color) =>{
    deleteOldActive();
    stock=color;
    document.getElementById('house').classList.add(color);
    document.getElementById(color).classList.add("active");    
};
// xóa active cũ
let deleteOldActive=()=>{
    document.getElementById(stock).classList.remove("active");
    document.getElementById('house').classList.remove(stock);
};


